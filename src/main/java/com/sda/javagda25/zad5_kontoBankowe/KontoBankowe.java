package com.sda.javagda25.zad5_kontoBankowe;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public long getNumerKonta() {
        return numerKonta;
    }

    public void wyswietlStanKonta() {
        System.out.println(stanKonta);
    }

    public void wplacSrodki(int ileWplacic) {
        stanKonta += ileWplacic;
    }

    public int pobierzSrodki(int ileWyplacic) {
        if (stanKonta >= ileWyplacic) { // albo jest tyle pieniedzy
            stanKonta -= ileWyplacic;
            return ileWyplacic;
        }

        // nie ma tyle pieniedzy, wyplacam tyle co mam
        int ileMogeWyplacic = stanKonta; //100
        stanKonta = 0; // wyzerowanie konta // 0
        return ileMogeWyplacic; // 100
    }
}
