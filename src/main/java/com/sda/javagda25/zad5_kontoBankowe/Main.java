package com.sda.javagda25.zad5_kontoBankowe;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(123L, 500);
        KontoBankowe kontoBeaty = new KontoBankowe(152L, 2500);

        wykonajPrzelew(kontoAndrzeja, kontoBeaty, 500);
        kontoBeaty.wyswietlStanKonta();
        kontoAndrzeja.wyswietlStanKonta();


    }

    // generowanie metody z zaznaczonego bloku kodu: ctrl + alt + m
    private static void wykonajPrzelew(KontoBankowe naJakieKonto, KontoBankowe zJakiegoKonta, int ilePieniedzy) {
        naJakieKonto.wplacSrodki(zJakiegoKonta.pobierzSrodki(ilePieniedzy));
        System.out.println("Wykonano przelew z konta o numerze: " + zJakiegoKonta.getNumerKonta() + " na konto o numerze: " +
                naJakieKonto.getNumerKonta() + " w kwocie: " + ilePieniedzy);
    }
}
