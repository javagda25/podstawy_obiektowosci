package com.sda.javagda25.dynamiczna_tablica_modyfikatory_dostepu;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        DynamicznaTablica tablica = new DynamicznaTablica();

        int linia;
        do {
            linia = scanner.nextInt();

            tablica.dodajElement(linia);
            System.out.println(tablica);

        } while (linia != 0);
    }
}
