package com.sda.javagda25.dynamiczna_tablica_modyfikatory_dostepu;

import java.util.Arrays;

public class DynamicznaTablica {
    private int[] tablica;

    private int iloscElementow;

    public DynamicznaTablica() {
        // tworzę tablicę o rozmiarze 10
        this.tablica = new int[10];
        this.iloscElementow = 0;
    }

    public void dodajElement(int element) {
        if (!czyStarczyMiejsca()) { // sprawdzenie czy jest miejsce
            rozszerzTabliceDwukrotnie();
        }

        tablica[iloscElementow] = element;
        iloscElementow++;
    }

    private void rozszerzTabliceDwukrotnie() {
        int[] nowaTablica = new int[tablica.length * 2];
        for (int i = 0; i < tablica.length; i++) {
            nowaTablica[i] = tablica[i];
        }

        tablica = nowaTablica;
    }

    private boolean czyStarczyMiejsca() {
        return iloscElementow < tablica.length;
    }

    @Override
    public String toString() {
        return "DynamicznaTablica{" +
                "tablica=" + Arrays.toString(tablica) +
                ", iloscElementow=" + iloscElementow +
                '}';
    }
}