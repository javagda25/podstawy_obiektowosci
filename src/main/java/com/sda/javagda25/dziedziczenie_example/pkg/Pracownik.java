package com.sda.javagda25.dziedziczenie_example.pkg;

import com.sda.javagda25.dziedziczenie_example.Osoba;

public class Pracownik extends Osoba {

    public Pracownik(String imie, String nazwisko) {
        super(imie, nazwisko);
    }

}

// private - tylko w klasie
// pkg prot (brak) - tylko w package'u
// protected - dodatkowo w klasa dziedziczących (z poza pakietu)
// public - wszędzie