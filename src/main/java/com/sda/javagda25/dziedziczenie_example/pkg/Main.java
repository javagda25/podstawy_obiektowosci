package com.sda.javagda25.dziedziczenie_example.pkg;

import com.sda.javagda25.dziedziczenie_example.Osoba;

public class Main {
    public static void main(String[] args) {
//        Osoba osoba = new Osoba();

        Pracownik pracownik = new Pracownik("Jan", "Kowalski");
        pracownik.przedstawSie();

        Manager manager = new Manager("Ania", "Kowalska");
        manager.przedstawSie();

        Osoba[] osoby = new Osoba[2];
        osoby[0] = pracownik;
        osoby[1] = manager;

        for (int i = 0; i < osoby.length; i++) {
            osoby[i].przedstawSie();
        }
    }
}
