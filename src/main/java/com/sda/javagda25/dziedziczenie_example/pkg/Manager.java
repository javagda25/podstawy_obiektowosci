package com.sda.javagda25.dziedziczenie_example.pkg;

import com.sda.javagda25.dziedziczenie_example.Osoba;

public class Manager extends Osoba {

    public Manager(String imie, String nazwisko) {
        super(imie, nazwisko);
    }
}
