package com.sda.javagda25.dziedziczenie_example;

public class Osoba {
    protected String imie;
    private String nazwisko;

    public Osoba(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void przedstawSie() {
        System.out.println("Jestem: " + imie + " " + getNazwisko());
    }
}
