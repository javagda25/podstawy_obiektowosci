package com.sda.javagda25.zad9_if_exception;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        // case 1:
        System.out.println(oblicz(a, b));

        // case 2:
        // tutaj świadomie nie przechwyciłem błędu - czyli w przypadku błędu aplikacja się 
        // zamknie
        System.out.println(obliczRuntimeException(a, b));

        // case 3:
        try {
            System.out.println(obliczException(a, b));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int oblicz(int a, int b) {
        if (b < 0) {
            System.out.println("Liczba b jest mniejsza od 0.");
            return 0;
        } else {
            return a / b;
        }
    }

    public static int obliczRuntimeException(int a, int b) {
        if (b < 0) {
            throw new RuntimeException("Liczba b jest mniejsza od 0.");
        } else {
            return a / b;
        }
    }

    public static int obliczException(int a, int b) throws Exception {
        if (b < 0) {
            throw new Exception("Liczba b jest mniejsza od 0.");
        } else {
            return a / b;
        }
    }
}
