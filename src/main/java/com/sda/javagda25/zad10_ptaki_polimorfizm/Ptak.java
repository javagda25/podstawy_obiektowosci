package com.sda.javagda25.zad10_ptaki_polimorfizm;

public class Ptak {
    public void spiewaj(){
        System.out.println("ćwir ćwir");
    }

    public void spiewaj(int ileRazy){
        for (int i = 0; i < ileRazy; i++) {
            spiewaj();
        }
    }

    public void spiewaj(int ileRazy, String co){
        for (int i = 0; i < ileRazy; i++) {
            spiewaj();
        }
    }

}
