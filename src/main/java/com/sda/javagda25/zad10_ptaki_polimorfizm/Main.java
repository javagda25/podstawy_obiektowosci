package com.sda.javagda25.zad10_ptaki_polimorfizm;

public class Main {
    public static void main(String[] args) {
        Ptak ptak = new Ptak();
        ptak.spiewaj();

        Kukulka kukulka = new Kukulka();
        kukulka.spiewaj();

        Bocian bocian = new Bocian();
        bocian.spiewaj();

        Sowa sowa = new Sowa();
        sowa.spiewaj();

        Ptak p = new Kukulka(); // polimorfizm // wielopostaciowość
        p.spiewaj(); // ? kuku kuku
    }
}
