package com.sda.javagda25.exception_dziedziczenie;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        try {
            System.out.println(podziel(a, b));
        } catch (CholeroException che) {
            System.out.println(che.getMessage());
        }

    }

    private static int podziel(int a, int b) throws CholeroException {
        if (b != 0) {
            return a / b;
        }
        throw new CholeroException();
    }
}
