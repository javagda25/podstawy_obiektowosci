package com.sda.javagda25.zad6_bilet;

public enum Bilet {
    ULGOWY_GODZINNY(1.60, 60),
    ULGOWY_CALODOBOWY(6.0, 1440),
    NORMALNY_GODZINNY(3.20, 60),
    NORMALNY_CALODOBOWY(12.0, 1440),
    BRAK_BILETU(0, 0);

    private double cena;
    private int czasJazdy;

    Bilet(double cena, int czasJazdy) {
        this.cena = cena;
        this.czasJazdy = czasJazdy;
    }

    public double pobierzCene() {
        return cena;
    }

    public int pobierzCzasJazdy() {
        return czasJazdy;
    }

    // toString() - w enum zwraca nazwę enuma.
    public void wyswietlDaneOBilecie() {
        // wywołanie metody na bilecie BRAK_BILETU zwróci:
        // "Bilet: BRAK_BILETU - cena 0.0 czas jazdy: 0"
        // "Bilet: NORMALNY_CALODOBOWY - cena 12.0 czas jazdy: 1440"
        System.out.println("Bilet: " + this + " - cena: " + cena + " czas jazdy: " + czasJazdy);
    }
}
