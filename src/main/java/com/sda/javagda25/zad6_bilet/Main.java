package com.sda.javagda25.zad6_bilet;

public class Main {
    public static void main(String[] args) {
        Bilet.NORMALNY_GODZINNY.wyswietlDaneOBilecie();
    }

    private static Bilet pobierzBilet(int wiek, int czasJazdy, double kwota) {
        if (wiek < 15) { // ulgowy
            if (czasJazdy <= Bilet.ULGOWY_GODZINNY.pobierzCzasJazdy()) {
                if (kwota >= Bilet.ULGOWY_GODZINNY.pobierzCene()) {
                    return Bilet.ULGOWY_GODZINNY;
                }
            } else {
                if (kwota >= Bilet.ULGOWY_CALODOBOWY.pobierzCene()) {
                    return Bilet.ULGOWY_CALODOBOWY;
                }
            }
        } else {
            if (czasJazdy <= Bilet.NORMALNY_GODZINNY.pobierzCzasJazdy()) {
                if (kwota >= Bilet.NORMALNY_GODZINNY.pobierzCene()) {
                    return Bilet.NORMALNY_GODZINNY;
                }
            } else {
                if (kwota >= Bilet.NORMALNY_CALODOBOWY.pobierzCene()) {
                    return Bilet.NORMALNY_CALODOBOWY;
                }
            }
        }
        return Bilet.BRAK_BILETU;
    }

}
