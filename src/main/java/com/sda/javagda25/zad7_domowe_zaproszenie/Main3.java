package com.sda.javagda25.zad7_domowe_zaproszenie;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile zaproszeń?");
        int ileZaproszen = scanner.nextInt();

        Zaproszenie[] zaproszenies = new Zaproszenie[ileZaproszen];

        for (int i = 0; i < ileZaproszen; i++) {
            System.out.println("Podaj imie:");
            String imie = scanner.next();

            System.out.println("Podaj nazwisko:");
            String nazwisko = scanner.next();

            System.out.println("Podaj ilosc osob:");
            int iloscOsobNaZaproszeniu = scanner.nextInt();
            TypZaproszenia typZaproszeniaEnum = TypZaproszenia.getTypZaproszenia(iloscOsobNaZaproszeniu);

            zaproszenies[i] = new Zaproszenie(imie, nazwisko, typZaproszeniaEnum);
        }

        // zebrana informacja.
        int sumaOsob = 0;
        for (int i = 0; i < ileZaproszen; i++) {
            sumaOsob += zaproszenies[i].getTypZaproszenia().getIloscOsob();
        }

        System.out.println("Zaproszono w sumie: " + sumaOsob);
    }
}
