package com.sda.javagda25.zad7_domowe_zaproszenie;

public enum TypZaproszenia {
    POJEDYNCZE(1),
    Z_OS_TOWARZYSZACA(2);

    private int iloscOsob;

    TypZaproszenia(int iloscOsob) {
        this.iloscOsob = iloscOsob;
    }

    public int getIloscOsob() {
        return iloscOsob;
    }

    public static TypZaproszenia getTypZaproszenia(int iloscOsob) {
        if (iloscOsob == 1) {
            return TypZaproszenia.POJEDYNCZE;
        } else if (iloscOsob == 2) {
            return TypZaproszenia.Z_OS_TOWARZYSZACA;
        }
        throw new RuntimeException("Niepoprawna ilosc osob!");
    }
}
