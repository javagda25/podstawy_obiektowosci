package com.sda.javagda25.zad7_domowe_zaproszenie;

public class Zaproszenie {
    private String imie;
    private String nazwisko;
    private TypZaproszenia typZaproszenia;

    public Zaproszenie() {
    }

    public Zaproszenie(String imie, String nazwisko, TypZaproszenia typZaproszenia) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.typZaproszenia = typZaproszenia;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public TypZaproszenia getTypZaproszenia() {
        return typZaproszenia;
    }

    public void setTypZaproszenia(TypZaproszenia typZaproszenia) {
        this.typZaproszenia = typZaproszenia;
    }

    @Override
    public String toString() {
        return "Zaproszenie{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", typZaproszenia=" + typZaproszenia +
                '}';
    }
}
