package com.sda.javagda25.zad7_domowe_zaproszenie;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile zaproszeń?");
        int ileZaproszen = scanner.nextInt();

        Zaproszenie[] zaproszenies = new Zaproszenie[ileZaproszen];

        for (int i = 0; i < ileZaproszen; i++) {
            zaproszenies[i] = new Zaproszenie();
            System.out.println("Podaj imie:");
            zaproszenies[i].setImie(scanner.next());

            System.out.println("Podaj nazwisko:");
            zaproszenies[i].setNazwisko(scanner.next());

            System.out.println("Podaj typ zaproszenia:");
            String typZaproszenia = scanner.next();
            TypZaproszenia typZaproszeniaEnum = TypZaproszenia.valueOf(typZaproszenia.toUpperCase());
            zaproszenies[i].setTypZaproszenia(typZaproszeniaEnum);
        }
        
        // zebrana informacja.
        int sumaOsob = 0;
        for (int i = 0; i < ileZaproszen; i++) {
            sumaOsob += zaproszenies[i].getTypZaproszenia().getIloscOsob();
        }

        System.out.println("Zaproszono w sumie: " + sumaOsob);
    }
}
