package com.sda.javagda25.zad11_komputer_laptop;

public class Main {
    public static void main(String[] args) {
        Komputer[] komputers = new Komputer[5];

        komputers[0] = new Laptop(100, "Mak", TypProcesora.JEDNORDZENIOWY, 20.3, false);
        komputers[1] = new Laptop(20, "Del", TypProcesora.WIELORDZENIOWY, 51.3, false);
        komputers[2] = new Komputer(50, "Asus", TypProcesora.JEDNORDZENIOWY);
        komputers[3] = new Laptop(83, "Hape", TypProcesora.WIELORDZENIOWY, 21.3, false);
        komputers[4] = new Komputer(91, "Mak", TypProcesora.JEDNORDZENIOWY);


        int i = 0;
        while (i<komputers.length){
            System.out.println(komputers[i]);


            if(komputers[i] instanceof Komputer){
                System.out.println("Jest komuterem.");
            }
            // sprawdzenie czy dany komputer
            // jest laptopem
            //
            // robimy rzutowanie po upewnieniu sie
            if(komputers[i] instanceof Laptop){
                Laptop l = (Laptop) komputers[i];
                l.otworzLaptopa();
                System.out.println("Jest laptopem.");
            }
            i++;
        }



    }
}
