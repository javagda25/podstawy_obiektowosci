package com.sda.javagda25.zad1_enum;

public enum TypPojazdu {
    ROWER(2, "a"),
    SAMOCHOD(4, "b"),
    TROJKOLOWIEC(3,"fiat");

    private int iloscKol;
    private String kolor;

    TypPojazdu(int iloscKol, String kolor) {
        this.iloscKol = iloscKol;
        this.kolor = kolor;
    }

    public int getIloscKol() {
        return iloscKol;
    }

}
