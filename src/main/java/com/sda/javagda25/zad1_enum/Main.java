package com.sda.javagda25.zad1_enum;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Osoba x = new Osoba();

        String imie = scanner.next();
        x.setImie(imie);

        int rok = Integer.parseInt(scanner.next());
        String plec = scanner.next();
        Plec plecEnum = Plec.valueOf(plec.toUpperCase());

        Osoba o = new Osoba(imie,
                rok,
                plecEnum);

        System.out.println(o);

    }
}
