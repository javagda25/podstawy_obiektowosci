package com.sda.javagda25.zad1_enum;

public class Osoba {
    private String imie;
    private int rokUrodzenia;
    private Plec plec;

    public Osoba(String imie, int rokUrodzenia, Plec plec) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
        this.plec = plec;
    }

    public Osoba(String imie, int rokUrodzenia) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
    }

    public Osoba() {
    }

    public void przedstawSie() {
        System.out.println("Cześć! Mam na imie " + imie + " i mam " + (2019 - rokUrodzenia) + " lat.");
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setRokUrodzenia(int rokUrodzenia) {
        this.rokUrodzenia = rokUrodzenia;
    }

    // setter
    public void setImie(String naco) {
        this.imie = naco;
    }

    // getter
    public String getImie(){
        return imie;
    }

    //private - dostępne wewnątrz tej klasy
    //protected - dostępne w obrębie tej klasy, pakietu i klas dziedziczących
    //brak - package protected - dostępne w obrębie klasy i pakietu


    @Override
    public String toString() {
        String original = super.toString();
        // ^^ pobranie treści metody toString z klasy nadrzędnej.

        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", rokUrodzenia=" + rokUrodzenia +
                ", plec=" + plec +
                '}' + original;
    }
}
