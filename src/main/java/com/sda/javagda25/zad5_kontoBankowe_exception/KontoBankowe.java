package com.sda.javagda25.zad5_kontoBankowe_exception;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public long getNumerKonta() {
        return numerKonta;
    }

    public void wyswietlStanKonta() {
        System.out.println(stanKonta);
    }

    public void wplacSrodki(int ileWplacic) throws Exception {
        if (ileWplacic < 0) {
            throw new Exception("Ujemna kwota wpłaty");
        }
        stanKonta += ileWplacic;
    }

    public int pobierzSrodki(int ileWyplacic) throws Exception {
        if (ileWyplacic < 0) {
            throw new RuntimeException("Próba wypłacenia ujemnej kwoty.");
        }

        if (stanKonta >= ileWyplacic) { // albo jest tyle pieniedzy
            stanKonta -= ileWyplacic;
            return ileWyplacic;
        }

        throw new Exception("Brak środków na koncie");
    }
}
