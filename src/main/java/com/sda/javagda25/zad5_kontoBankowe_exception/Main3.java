package com.sda.javagda25.zad5_kontoBankowe_exception;

import java.util.Scanner;

public class Main3 {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(123L, 1000);
        KontoBankowe kontoBeaty = new KontoBankowe(152L, 1000);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kwote:");
        int iloscPieniedzy = scanner.nextInt();

        try {
            kontoAndrzeja.pobierzSrodki(iloscPieniedzy);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        kontoBeaty.wyswietlStanKonta();
        kontoAndrzeja.wyswietlStanKonta();

    }

    // generowanie metody z zaznaczonego bloku kodu: ctrl + alt + m
    private static void wykonajPrzelew(KontoBankowe naJakieKonto, KontoBankowe zJakiegoKonta, int ilePieniedzy) throws Exception {
        naJakieKonto.wplacSrodki(zJakiegoKonta.pobierzSrodki(ilePieniedzy));
        System.out.println("Wykonano przelew z konta o numerze: " + zJakiegoKonta.getNumerKonta() + " na konto o numerze: " +
                naJakieKonto.getNumerKonta() + " w kwocie: " + ilePieniedzy);
    }
}
