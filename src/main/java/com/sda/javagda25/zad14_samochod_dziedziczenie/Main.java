package com.sda.javagda25.zad14_samochod_dziedziczenie;

public class Main {
    public static void main(String[] args) {
        Kabriolet kabriolet = new Kabriolet("czerwony", "fiat", 1983);
        System.out.println("Swiatla wlaczone: " + kabriolet.czySwiatlaWlaczone());
        kabriolet.wlaczSwiatla();
        System.out.println("Swiatla wlaczone: " + kabriolet.czySwiatlaWlaczone());

        System.out.println("Dach : " + kabriolet.czyDachSchowany());
        kabriolet.schowajDach();
        System.out.println("Dach : " + kabriolet.czyDachSchowany());

        kabriolet.przyspiesz();//10
        kabriolet.przyspiesz();//20
        kabriolet.przyspiesz();//30
        kabriolet.przyspiesz();//40
        kabriolet.przyspiesz();//50
        kabriolet.przyspiesz();//60
        kabriolet.przyspiesz();//70
        kabriolet.przyspiesz();//80
        System.out.println("Kabriolet: " + kabriolet);

        kabriolet.przyspiesz();//90
        kabriolet.przyspiesz();//100
        kabriolet.przyspiesz();//110
        kabriolet.przyspiesz();//120
        kabriolet.przyspiesz();//120
        System.out.println("Kabriolet: " + kabriolet);
    }
}
