package com.sda.javagda25.zad14_samochod_dziedziczenie;

public class Main2 {
    public static void main(String[] args) {
        Kabriolet kabriolet1 = new Kabriolet("czerwony", "fiat", 1983);
        Kabriolet kabriolet2 = new Kabriolet("czerwony", "fiat", 1983);

        System.out.println("1: " + kabriolet1.hashCode());
        System.out.println("2: " + kabriolet2.hashCode());
        System.out.println(kabriolet1.equals(kabriolet2));
    }
}
