package com.sda.javagda25.zad14_samochod_dziedziczenie;

import java.util.Objects;

public class Samochod {
    protected int predkosc;
    private boolean czySwiatlaWlaczone;

    protected String kolor, marka;
    protected int rocznik; // przestaje być zmienną, staje się stałą!

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz() {
        predkosc += 10;
        if (predkosc > 120) {
            predkosc = 120;
        }
        System.out.println("Przyspieszam do " + predkosc + " km/h.");
    }

    public void wlaczSwiatla() {
        czySwiatlaWlaczone = true;
    }

    public boolean czySwiatlaWlaczone() {
        return czySwiatlaWlaczone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;         // sprawdzenie czy obiekty są identyczne (parametr o == this)
        if (!(o instanceof Samochod)) return false; // sprwdzenie czy obiekt o jest tego samego typu co this
                                                    // jeśli nie jest samochodem a jest np. paczką czipsów
                                                    // to otrzymamy false.
        Samochod samochod = (Samochod) o;           // rzutujemy sprawdzony obiekt na typ (samochód)
        return rocznik == samochod.rocznik &&
                kolor.equals(samochod.kolor) &&
                marka.equals(samochod.marka);   // porównujemy poszczególne pola obiektów!!!
    }

    @Override
    public int hashCode() {
        return Objects.hash(kolor, marka, rocznik);
    }

    @Override
    public String toString() {
        return kolor + " samochód marki " + marka + " rocznik " + rocznik;
    }
}
