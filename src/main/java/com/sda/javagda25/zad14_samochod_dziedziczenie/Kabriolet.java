package com.sda.javagda25.zad14_samochod_dziedziczenie;

import java.util.Objects;

public class Kabriolet extends Samochod {
    private boolean czyDachSchowany;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    public void schowajDach() {
        czyDachSchowany = true;
    }

    public boolean czyDachSchowany() {
        return czyDachSchowany;
    }

    @Override
    public void przyspiesz() {
        predkosc += 10;
        if (predkosc > 180) {
            predkosc = 180;
        }
        System.out.println("Przyspieszam do " + predkosc + " km/h.");
    }

    //    Do usunięcia
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Kabriolet)) return false;
//        Kabriolet kabriolet = (Kabriolet) o;
//        return czyDachSchowany == kabriolet.czyDachSchowany;
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(czyDachSchowany);
//    }

    @Override
    public String toString() {
        return super.toString() + " z rozsuwanym dachem.";
    }
}
