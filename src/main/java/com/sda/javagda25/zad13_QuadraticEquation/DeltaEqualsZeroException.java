package com.sda.javagda25.zad13_QuadraticEquation;

public class DeltaEqualsZeroException extends Exception {
    public DeltaEqualsZeroException() {
        super("There is only one solution. Use getX1() to access x0 solution.");
    }
}
