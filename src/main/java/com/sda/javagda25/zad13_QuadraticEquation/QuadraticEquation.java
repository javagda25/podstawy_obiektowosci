package com.sda.javagda25.zad13_QuadraticEquation;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getX1() throws DeltaLessThanZeroException {
        double delta = calculateDelta();
        if (delta == 0) {
            return (-b / (2 * a));
        } else if (delta > 0) {
            return ((-b - Math.sqrt(delta)) / (2 * a));
        } else { // delta < 0;
            throw new DeltaLessThanZeroException();
        }
    }

    private double calculateDelta() {
        return (b * b) - (4 * a * c);
    }

    public double getX2() throws DeltaEqualsZeroException, DeltaLessThanZeroException {
        double delta = calculateDelta();
        if (delta == 0) {
            throw new DeltaEqualsZeroException();
        } else if (delta > 0) {
            return ((-b + Math.sqrt(delta)) / (2 * a));
        } else {
            throw new DeltaLessThanZeroException();
        }
    }
}
