package com.sda.javagda25.zad13_QuadraticEquation;

public class Main {
    public static void main(String[] args) {
//        QuadraticEquation quadraticEquation = new QuadraticEquation(1, 2, 1);
        QuadraticEquation quadraticEquation = new QuadraticEquation(2, 2, 2);
        try {
            System.out.println(quadraticEquation.getX1());
        } catch (DeltaLessThanZeroException e) {
            System.err.println(e.getMessage());
        }
        try {
            System.out.println(quadraticEquation.getX2());
        } catch (DeltaEqualsZeroException e) {
            System.err.println(e.getMessage());
        } catch (DeltaLessThanZeroException e) {
            System.err.println(e.getMessage());
        }
    }
}
