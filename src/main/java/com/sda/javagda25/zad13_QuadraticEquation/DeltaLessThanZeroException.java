package com.sda.javagda25.zad13_QuadraticEquation;

public class DeltaLessThanZeroException extends Exception {
    public DeltaLessThanZeroException() {
        super("Delta is less than zero. There are no solutions to this equation.");
    }
}
