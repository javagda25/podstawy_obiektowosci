package com.sda.javagda25.zad1_prezentacja;

import com.sda.javagda25.zad1_prezentacja.pkg.Osoba;

public class Main {
    public static void main(String[] args) {
        Osoba osobaAnia = new Osoba("a", 1994);

//        osobaAnia.imie = "Ania";
//        osobaAnia.rokUrodzenia = 1994;

        System.out.println(osobaAnia);
//        System.out.println(osobaAnia.imie);
        System.out.println(osobaAnia.getImie());
//        System.out.println(osobaAnia.rokUrodzenia);

        osobaAnia.przedstawSie();

        Osoba osobaAndrzej = new Osoba("Andrzej", 1965);
        osobaAndrzej.setImie("Andrzej");
//        osobaAndrzej.rokUrodzenia = 1965;
        osobaAndrzej.setRokUrodzenia(1965);

        osobaAndrzej.przedstawSie();


        osobaAnia.setImie("Nie Ania");
        osobaAndrzej.setImie("Nie Andrzej");

    }
}
