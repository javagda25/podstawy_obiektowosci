package com.sda.javagda25.zad4_domowe;

public class PoziomGry {
    private String nazwa;
    private int iloscPotworow;
    private PoziomTrudnosci poziom; // alt + enter (szukamy linii z generuj enum)
    private int dlugoscPoziomu;
    private int limitCzasu;
    private int punktowZaPrzejscie;

    public PoziomGry() {
    }

    public PoziomGry(String nazwa, int iloscPotworow, PoziomTrudnosci poziom, int dlugoscPoziomu, int limitCzasu, int punktowZaPrzejscie) {
        this.nazwa = nazwa;
        this.iloscPotworow = iloscPotworow;
        this.poziom = poziom;
        this.dlugoscPoziomu = dlugoscPoziomu;
        this.limitCzasu = limitCzasu;
        this.punktowZaPrzejscie = punktowZaPrzejscie;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getIloscPotworow() {
        return iloscPotworow;
    }

    public void setIloscPotworow(int iloscPotworow) {
        this.iloscPotworow = iloscPotworow;
    }

    public PoziomTrudnosci getPoziom() {
        return poziom;
    }

    public void setPoziom(PoziomTrudnosci poziom) {
        this.poziom = poziom;
    }

    public int getDlugoscPoziomu() {
        return dlugoscPoziomu;
    }

    public void setDlugoscPoziomu(int dlugoscPoziomu) {
        this.dlugoscPoziomu = dlugoscPoziomu;
    }

    public int getLimitCzasu() {
        return limitCzasu;
    }

    public void setLimitCzasu(int limitCzasu) {
        this.limitCzasu = limitCzasu;
    }

    public int getPunktowZaPrzejscie() {
        return punktowZaPrzejscie;
    }

    public void setPunktowZaPrzejscie(int punktowZaPrzejscie) {
        this.punktowZaPrzejscie = punktowZaPrzejscie;
    }

    @Override
    public String toString() {
        return "PoziomGry{" +
                "nazwa='" + nazwa + '\'' +
                ", iloscPotworow=" + iloscPotworow +
                ", poziom=" + poziom +
                ", dlugoscPoziomu=" + dlugoscPoziomu +
                ", limitCzasu=" + limitCzasu +
                ", punktowZaPrzejscie=" + punktowZaPrzejscie +
                '}';
    }
}
