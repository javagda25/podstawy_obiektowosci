package com.sda.javagda25.zad4_domowe;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PoziomGry poziomGry = new PoziomGry();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj nazwę poziomu:");
        poziomGry.setNazwa(scanner.nextLine());

        System.out.println("Podaj ilość potworów:");
        poziomGry.setIloscPotworow(Integer.parseInt(scanner.nextLine()));

        System.out.println("Podaj poziom trudności:");
        // todo: wypisać wszystkie możliwe opcje
        String poziomTrudnosci = scanner.nextLine();
        PoziomTrudnosci poziom = PoziomTrudnosci.valueOf(poziomTrudnosci);
        poziomGry.setPoziom(poziom);

        System.out.println("Podaj długość poziomu:");
        poziomGry.setDlugoscPoziomu(Integer.parseInt(scanner.nextLine()));

        System.out.println("Podaj limit czasu:");
        poziomGry.setLimitCzasu(Integer.parseInt(scanner.nextLine()));

        System.out.println("Podaj ilość punktów za przejście poziomu:");
        poziomGry.setPunktowZaPrzejscie(Integer.parseInt(scanner.nextLine()));

        // stworzono obiekt.
        System.out.println(poziomGry);
    }
}
