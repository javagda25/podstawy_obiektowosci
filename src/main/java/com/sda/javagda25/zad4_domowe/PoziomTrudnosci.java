package com.sda.javagda25.zad4_domowe;

public enum PoziomTrudnosci {
    LOW(1),
    SEMI_MEDIUM(2),
    MEDIUM(3),
    SEMI_PRO(4),
    PRO(5),
    ADVANCED(6),
    CHUCK_NORRIS(69);

    private int level;

    PoziomTrudnosci(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
