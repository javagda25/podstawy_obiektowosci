package com.sda.javagda25.zad4_domowe;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj nazwę poziomu:");
        String nazwa = scanner.nextLine();

        System.out.println("Podaj ilość potworów:");
        int iloscPotworow = Integer.parseInt(scanner.nextLine());

        System.out.println("Podaj poziom trudności:");
        // todo: wypisać wszystkie możliwe opcje

        wypiszOpcjePoziomTrudnosci();

        String poziomTrudnosci = scanner.nextLine();
        PoziomTrudnosci poziom = PoziomTrudnosci.valueOf(poziomTrudnosci);

        System.out.println("Podaj długość poziomu:");
        int dlugoscPoziomu = Integer.parseInt(scanner.nextLine());

        System.out.println("Podaj limit czasu:");
        int limitCzasu = Integer.parseInt(scanner.nextLine());

        System.out.println("Podaj ilość punktów za przejście poziomu:");
        int punktowZaPrzejscie = Integer.parseInt(scanner.nextLine());

        // stworzono obiekt.

        PoziomGry poziomGry = new PoziomGry(nazwa, iloscPotworow, poziom, dlugoscPoziomu, limitCzasu, punktowZaPrzejscie);
//        PoziomGry poziomGry = new PoziomGry(scanner.nextLine(), Integer.parseInt(scanner.nextLine()), poziom, dlugoscPoziomu, limitCzasu, punktowZaPrzejscie);

        System.out.println(poziomGry);

    }

    private static void wypiszOpcjePoziomTrudnosci() {
        PoziomTrudnosci[] tablicaOpcji = PoziomTrudnosci.values();
        System.out.println("Dostępne możliwości:");
        for (int i = 0; i < tablicaOpcji.length; i++) {
            System.out.println("Opcja: " + tablicaOpcji[i] + " - " + tablicaOpcji[i].getLevel());
        }
    }

    private static void wypiszOpcjePoziomTrudnosci2() {
        System.out.println("Dostępne możliwości:");
        for (int i = 0; i < PoziomTrudnosci.values().length; i++) {
            System.out.println("Opcja: " + PoziomTrudnosci.values()[i] + " - " + PoziomTrudnosci.values()[i].getLevel());
        }
    }
}
