package com.sda.javagda25.exception_example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        System.out.println(zwrocZeScannera());
        try { // try - spróbuj wykonać blok kodu
            System.out.println("1");

            System.out.println("2");
            dzielenie(5, 0);

            System.out.println("3");
            // do tego miejsca
        } catch (NumberFormatException e) { // jeśli nie uda Ci się z powodu złapania Exception <
            System.err.println("Jest błąd :(");// wykonaj ten blok kodu.
            System.out.println(e.getMessage());
        } catch (ArithmeticException ae) {
            System.err.println("inny blad");
            System.out.println(ae.getMessage());
        }
        System.out.println("cośtam");
    }

    private static double dzielenie(int a, int b) /*throws Exception*/ {
        if (b == 0) {
            throw new NumberFormatException();
        }
        return a / b; // RuntimeException
    }

    private static int zwrocZeScannera() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
//            try {
            String text = scanner.next();
            int liczba = Integer.parseInt(text); // RuntimeException
            return liczba;
//            } catch (Exception e) {
//                System.err.println("Błąd wejścia, try again.");
//            }
        }
    }
}
