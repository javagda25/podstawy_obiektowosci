package com.sda.javagda25.zad2;

public class Student {
    private int numerIndeksu;
    private String imie;
    private String nazwisko;
    private String plec;

    public Student() {
    }

    public Student(int numerIndeksu, String imie, String nazwisko, String plec) {
        this.numerIndeksu = numerIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }

    public void przedstawSie() {
        String plecL;
        if (this.plec.equals("k")) {
            plecL = "kobieta";
        } else {
            plecL = "mezczyzna";
        }
        System.out.println("Cześć, jestem " + imie + " " + nazwisko + " jestem " + plecL + " mój numer indeksu to: " + numerIndeksu);
    }

    public int getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(int numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numerIndeksu=" + numerIndeksu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", plec='" + plec + '\'' +
                '}';
    }
}
