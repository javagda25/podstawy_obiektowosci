package com.sda.javagda25.zad2;

import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie:");
        String imie = scanner.next();
        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.next();

        String indeks;
        int indeksInt;
        do {
            System.out.println("Podaj indeks:");
            indeks = scanner.next();

            // parsowanie int - przetworzenie ciągu znaków (liczby ze string'a 'indeks') na
            //                  zmienną typu int.
            indeksInt = Integer.parseInt(indeks);
        } while (indeks.length() != 6);

        String plec;
        do {
            System.out.println("Podaj plec:");
            plec = scanner.next().toLowerCase();
        } while (!plec.equals("k") && !plec.equals("m"));

        Student student = new Student(indeksInt, imie, nazwisko, plec);

        System.out.println(student.getImie());
        System.out.println(student.getNazwisko());
        System.out.println(student.getNumerIndeksu());
        System.out.println(student.getPlec());

        // wypisanie (automatycznie) generuje tekst z metody toString()
        System.out.println("student=" + student);

        // obfuskator
    }
}
