package com.sda.javagda25.zad12_adult;

public class Club {
    public void enter(Person person) throws NoAdultException {
        if (person.getWiek() >= 18) {
            System.out.println("Możesz wejść.");
        }else{
            throw new NoAdultException();
        }
    }
}
