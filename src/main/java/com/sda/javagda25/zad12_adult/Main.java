package com.sda.javagda25.zad12_adult;

public class Main {
    public static void main(String[] args) {
        Club club = new Club();

        Person person = new Person("A", "B", 18);

        try {
            club.enter(person);
        } catch (NoAdultException e) {
            System.out.println(e.getMessage());
        }
    }
}
