package com.sda.javagda25.zad12_adult;

public class NoAdultException extends Exception{
    public NoAdultException() {
        super("Nie możesz wejść dzieciaku!");
    }
}
